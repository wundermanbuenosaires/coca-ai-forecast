import warnings
import itertools
import logging
import os
import cPickle
import sys

import numpy as np
import pandas as pd

from sklearn.base import BaseEstimator
from sklearn.base import TransformerMixin
from sklearn.pipeline import Pipeline
from sklearn.pipeline import FeatureUnion
from sklearn.preprocessing import MultiLabelBinarizer
from sklearn.preprocessing import Imputer
from sklearn.model_selection import train_test_split
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import cross_val_score
from sklearn.linear_model import LinearRegression
from sklearn.tree import DecisionTreeRegressor
from sklearn.ensemble import RandomForestRegressor
from sklearn.ensemble import GradientBoostingRegressor
from sklearn.metrics import mean_squared_error
from sklearn.externals import joblib

from features_values import FEATURES


warnings.filterwarnings(action="ignore", module="scipy", message="^internal gelsd")
FORMAT = '[%(asctime)s] %(levelname)s %(message)s'
logging.basicConfig(format=FORMAT, datefmt='%H:%M:%S', level=logging.DEBUG)

FILE_NAME = 'dummy_dataset.csv'
ALGO_NAME = 'forest_reg.pkl'
SCORES_NAME = 'forest_reg_rmse_scores.pkl'
LABEL = 'Engagement score'


class DataFrameSelector(BaseEstimator, TransformerMixin):
    def __init__(self, attribute_names):
        self.attribute_names = attribute_names
    def fit(self, X, y=None):
        return self
    def transform(self, X):
        if isinstance(self.attribute_names, list):
            return X[self.attribute_names].values
        else:
            return X[self.attribute_names].values.reshape(-1, 1)


class CustomMultiLabelBinarizer(BaseEstimator, TransformerMixin):
    def __init__(self, classes=None, sparse_output=False):
        self.classes = classes
        self.sparse_output = sparse_output
    def fit(self, y):
        if self.classes is None:
            classes = sorted(set(itertools.chain.from_iterable(y)))
        else:
            classes = self.classes
        dtype = np.int if all(isinstance(c, int) for c in classes) else object
        self.classes_ = np.empty(len(classes), dtype=dtype)
        self.classes_[:] = classes
        return self
    def transform(self, X, y=None):
        return MultiLabelBinarizer(sparse_output=False).fit_transform(X)


def load_dataset(FILE_NAME):
    return pd.read_csv(FILE_NAME, sep=',', encoding='utf-8')


def add_missing_one_hot_feat(dataset_prepared, combinations_prepared):
    missing_feats = set(dataset_prepared) - set(combinations_prepared)
    for feat in missing_feats:
        combinations_prepared[feat] = 0
    combinations_prepared.sort_index(axis=1, inplace=True)
    return combinations_prepared.values


def display_scores(scores):
    print "Scores: %s" % scores
    print "Mean: %s"  % scores.mean()
    print "Standard deviation: %s" % scores.std()


def display_feature_importances(clf, dataset_prepared, top_features):
    feature_importances = clf.feature_importances_
    most_important_features = sorted(zip(feature_importances, list(dataset_prepared)), reverse=True)[:top_features]
    for score, feature in most_important_features:
        print '%s: %s' % (feature, score)


def remove_parameters(features, parameters):
    return {k: v for (k, v) in features.iteritems() if k not in list(parameters)}


def add_parameters(combinations, parameters):
    for index, permutation in enumerate(combinations):
        for parameter, value in parameters.iteritems():
            combinations[index][parameter] = value
    return combinations


def get_sorted_predictions(combinations_prepared_miss_feat_added, combinations, forest_reg):
    predictions = list()
    for index, row in enumerate(combinations_prepared_miss_feat_added):
        predictions.append((combinations[index], forest_reg.predict(row.reshape(1, -1))[0]))
    return sorted(predictions, key=lambda x: x[1], reverse=True)


def get_json(df):
    return df.to_json(orient='records')


def filter_combinations(combinations):
    filtered_combinations = list()
    for combination in combinations:
        if combination.get('Channel') == 'Facebook' and combination.get('Content type') in ['Text + Image', 'Text + Video']:
            filtered_combinations.append(combination)
        if combination.get('Channel') == 'Instagram' and combination.get('Content type') in ['Text + Image', 'Text + Video']:
            filtered_combinations.append(combination)
        if combination.get('Channel') == 'Twitter' and combination.get('Content type') in ['Text', 'Text + Image', 'Text + Video']:
            filtered_combinations.append(combination)
        if combination.get('Channel') == 'YouTube' and combination.get('Content type') in ['Text + Image', 'Video']:
            filtered_combinations.append(combination)
        if combination.get('Channel') == 'Programmatic' and combination.get('Content type') in ['Text + Image', 'Text + Video']:
            filtered_combinations.append(combination)
        if combination.get('Channel') == 'SEM' and combination.get('Content type') in ['Text']:
            filtered_combinations.append(combination)
        if combination.get('Channel') == 'Others':
            filtered_combinations.append(combination)
    return filtered_combinations


def get_args():
    parameters = {
        'Age range': sys.argv[1],
        'Gender': sys.argv[2],
        'With whom': sys.argv[3],
        'Country': sys.argv[4],
        'Location': sys.argv[5],
    }
    return parameters


if __name__ == "__main__":
    ## Load dataset
    logging.info('Importing dataset %s...' % FILE_NAME)
    dataset = load_dataset(FILE_NAME)
    logging.info('Finish importing dataset %s' % FILE_NAME)

    ## Extract features
    logging.info('Extracting features...')
    cat_feat = list(dataset.select_dtypes(include=['object']))
    logging.info('Finish extracting features')

    ## Clean dataset
    logging.info('Cleaning dataset...')
    cat_pipeline = Pipeline([
            ('selector', DataFrameSelector(cat_feat)),
            ('custom_multilabel_transformer', CustomMultiLabelBinarizer()),
        ])

    lab_pipeline = Pipeline([
            ('selector', DataFrameSelector(LABEL)),
            ('imputer', Imputer(strategy="median")),
        ])

    dataset_prepared = cat_pipeline.fit_transform(dataset)
    label_prepared = lab_pipeline.fit_transform(dataset)
    dataset_prepared = pd.DataFrame(dataset_prepared, columns=list(cat_pipeline.named_steps['custom_multilabel_transformer'].classes_))
    logging.info('Finish cleaning dataset')

    # ## Model evaluation
    # Random forest regression
    logging.info('Training Random Forest algorithm with %s instances...' % dataset_prepared.shape[0])
    if not os.path.isfile(ALGO_NAME) or not os.path.isfile(SCORES_NAME):
        forest_reg = RandomForestRegressor()
        forest_reg.fit(dataset_prepared.values, label_prepared.ravel())
        forest_reg_scores = cross_val_score(forest_reg, dataset_prepared.values, label_prepared.ravel(), scoring="neg_mean_squared_error", cv=10)
        forest_reg_rmse_scores = np.sqrt(-forest_reg_scores)
        joblib.dump(forest_reg, ALGO_NAME)
        joblib.dump(forest_reg_rmse_scores, SCORES_NAME)
    else:
        forest_reg = joblib.load(ALGO_NAME)
        forest_reg_rmse_scores = joblib.load(SCORES_NAME)
    logging.info('Finish training Random Forest algorithm')
    display_scores(forest_reg_rmse_scores)
    display_feature_importances(forest_reg, dataset_prepared, 10)

    ## Generate ranked recommendations
    logging.info('Predicting %s for available feature combinations...' % LABEL)
    parameters = get_args()
    features = remove_parameters(FEATURES, parameters)
    combinations = [dict(zip(features, values)) for values in itertools.product(*features.values())]
    filtered_combinations = filter_combinations(combinations)
    combinations = add_parameters(filtered_combinations, parameters)

    df_combinations = pd.DataFrame(combinations, columns=sorted(list(FEATURES)))
    combinations_prepared = cat_pipeline.fit_transform(df_combinations)
    combinations_prepared = pd.DataFrame(combinations_prepared, columns=list(cat_pipeline.named_steps['custom_multilabel_transformer'].classes_))
    combinations_prepared_miss_feat_added = add_missing_one_hot_feat(dataset_prepared, combinations_prepared)
    sorted_predictions = get_sorted_predictions(combinations_prepared_miss_feat_added, combinations, forest_reg)
    logging.info('Finish predicting %s for available feature combinations...' % LABEL)

    ranking = pd.DataFrame([x[0] for x in sorted_predictions], columns=['Time of day', 'Weekday / weekend', 'Impression Device', 'Channel', 'Content type', 'Reasons for drinking', 'Product characteristics'])
    ranking['Predicted Engagement Score'] = [x[1] for x in sorted_predictions]
    print get_json(ranking.head(10))
