## Installing ##

Install all the requirements first


```
#!terminal

pip install -r requirements.txt

```

Run the script with command-line arguments

```
#!terminal

python forecast.py age_range gender with_whom country location

```

Example
```
#!terminal

python forecast.py '55-59 Yrs' 'Male' 'No One/I Was Alone' 'Australia' 'Away'
```
