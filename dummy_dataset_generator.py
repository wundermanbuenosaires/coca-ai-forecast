import random

import numpy as np
import pandas as pd

from features_values import FEATURES


LABEL = 'Engagement score'
N_ROWS = 1000
MAX_SCORE = 10


def generate_random_dataset(N_ROWS, FEATURES, MAX_SCORE):
    sorted_feature_names = sorted(list(FEATURES))
    dataset = pd.DataFrame(np.nan, index=range(N_ROWS), columns=sorted_feature_names)
    for feat, val in FEATURES.iteritems():
        col = [random.sample(val, 1)[0] for n in range(N_ROWS)]
        dataset[feat] = col
    dataset[LABEL] = np.random.uniform(low=0.0, high=MAX_SCORE, size=N_ROWS)
    return dataset


def write_csv(dataset):
    dataset.to_csv('dummy_dataset.csv', index=False, encoding='utf-8')


if __name__ == "__main__":
    dataset = generate_random_dataset(N_ROWS, FEATURES, MAX_SCORE)
    write_csv(dataset)
